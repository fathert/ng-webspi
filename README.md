# NgWebspi

A template WebSPi project that can be used as the starting point for a new WebSPi based app. This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 1.6.8. It includes:

## Packages
* [Bootstrap 4.0.0](https://getbootstrap.com) (included as SASS) for fonts, styles and layout
* [ngBootstrap 1](https://ng-bootstrap.github.io/#/home) for Angular friendly Bootstrap components (such as date pickers and dropdowns)
* [Fontawsesome 5](https://fontawesome.com) for icons and spinners
* [WebSPi Core](https://bitbucket.org/fathert/webspi-core) for access the WebSPi webservice

## CSS
CSS class extensions in `styles.scss` have been added to allow Angular validity highlighting for Bootstrap components.

## Pollyfills
Most pollyfills for IE are included.

## Build Tools
A Gulp script to create a Tomcat `.WAR` file is included to create a `.WAR` file from the `/dist` folder, which can be deployed directly to a Tomcat server when the project is built with the `build-prod` script.

## Development server
A `proxy-config.js` script is provided to proxy API requests to a remote backend (including support for using a corporate proxy, if configued), change this to point to your server. Run `yarn run start` for a dev server. Navigate to `http://localhost:8080/`. The app will automatically reload if you change any of the source files.

# Angular CLI Development

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `yarn run build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `-prod` flag for a production build (see below).

## Production Build

Run `yarn run build-prod` to build the project for production. This also performs linting and creates a `.WAR` file for deployment directly to a Tomcat server.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
