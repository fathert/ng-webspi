/* 
 * Copyright (C) 2015 Tim
 */

var gulp = require('gulp'),
    war = require('gulp-war'),
    zip = require('gulp-zip'),
    gutil = require('gulp-util'),
    fs = require('fs');

// Create a WAR file that can be deployed to Tomcat. 
gulp.task('war', [], function(){

 var package = JSON.parse(fs.readFileSync('./package.json')),
     displayName = `${package.description || package.name} (${package.version})`,
     warFile = `${package.name.trim()}.war`;
     //warFile = `${package.name.trim()}##${package.version.trim()}.war`;

 return gulp
        // Select everything in dist folder, except any existing WAR file.
        .src(['dist/**/*', '!dist/**/*.war'])
        .pipe(war({
            welcome: 'index.html',
            version: package.version,
            webappExtras: [`<error-page><error-code>404</error-code><location>/index.html</location></error-page>`],
            displayName: displayName,
        }))
        .pipe(zip(warFile))
        .pipe(gulp.dest("./dist"))
        .on('end', () => gutil.log(`WAR file ${warFile} created.`));
});

// Create a WAR file that can be deployed to Tomcat. 
gulp.task('default', ['war']);
