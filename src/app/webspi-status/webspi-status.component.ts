import { Component, OnInit } from '@angular/core';
import { WebspiSpService } from '@webspi/core';
import { Observable } from 'rxjs/Observable';

@Component({
  selector: 'app-webspi-status',
  templateUrl: './webspi-status.component.html',
  styleUrls: ['./webspi-status.component.css']
})
export class WebspiStatusComponent implements OnInit {

  status$: Observable<string> = this.testWebspi();

  constructor(private ws: WebspiSpService) {
  }

  ngOnInit() {
  }

  /*
    Call the WebSpi procedure resolver to resolve itself. This procedure
    should always be present and working on a rnning WebSpi system.
  */
  testWebspi() {
    return this.ws.execute('WSP_SP_RslvProc', {
      NAME_: 'GBWP_SP_RSLVPROC',
      BASICRESOLVE_: '1',
    })
    .map(result => 'up')
    .catch((e, c) => Observable.of('down'));
  }
}
