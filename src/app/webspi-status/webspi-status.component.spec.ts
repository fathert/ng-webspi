import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WebspiStatusComponent } from './webspi-status.component';

describe('WebspiStatusComponent', () => {
  let component: WebspiStatusComponent;
  let fixture: ComponentFixture<WebspiStatusComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WebspiStatusComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WebspiStatusComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
