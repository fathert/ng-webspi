import { Component } from '@angular/core';
import { WebspiSpService } from '@webspi/core';
import { IconRegistrationService } from './icon-registration.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  constructor(
    private ws: WebspiSpService,
    private icons: IconRegistrationService
  ) {
    this.ws.loginUrl = '/';
    this.ws.connection = 'db2-i';
  }
}
