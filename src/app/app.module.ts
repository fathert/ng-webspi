import { BrowserModule } from '@angular/platform-browser';
import { RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { ServiceWorkerModule } from '@angular/service-worker';
import { WebspiCoreModule } from '@webspi/core';
import { AppComponent } from './app.component';
import { HomePageComponent } from './home-page/home-page.component';
import { HttpClientModule } from '@angular/common/http';
import { APP_ROUTES } from './app-router-config';
import { WebspiStatusComponent } from './webspi-status/webspi-status.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { IconRegistrationService } from './icon-registration.service';
import { environment } from '../environments/environment';

@NgModule({
  declarations: [
    AppComponent,
    HomePageComponent,
    WebspiStatusComponent
  ],
  imports: [
    HttpClientModule,
    BrowserModule,
    RouterModule.forRoot(APP_ROUTES),
    ServiceWorkerModule.register('./ngsw-worker.js', { enabled: environment.production }),
    NgbModule.forRoot(),
    WebspiCoreModule.forRoot(),
  ],
  providers: [
    IconRegistrationService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
