import { HomePageComponent } from './home-page/home-page.component';

export const APP_ROUTES = [
  { path: '', component: HomePageComponent }
];
