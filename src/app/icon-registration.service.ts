import { Injectable } from '@angular/core';
import fontawesome from '@fortawesome/fontawesome';
import faCheck from '@fortawesome/fontawesome-free-solid';

@Injectable()
export class IconRegistrationService {

  constructor() {
    fontawesome.library.add(faCheck);
   }

}
